require 'prime'

class PrimeFactor
  SMALLEST_PRIME = 2
  NEXT_PRIME = 3
  
  def initialize(number)
    @number = number
    @result = []
  end
  
  # 1. Find the modulo : 49 % 3
  # 2. Check the remainder is 1
  # 3. Find the next number from 3 which is a prime
  # 4. Find the modulo : 49 % 5 
  # 5. If non-zero, reject
  # 6. If zero, then use that prime number to divide the 49
  # 7. remainder * that_number is the intermediate result
  
  def calculate
    # first step
    if @number % SMALLEST_PRIME == 0
      remainder = @number / SMALLEST_PRIME
    
      prime_factorial(SMALLEST_PRIME, remainder)
    else
      remainder = @number / NEXT_PRIME
 
      prime_factorial(NEXT_PRIME, remainder)
    end
    @result
  end
  
  private
  
  def prime?(number)
    Prime.prime?(number) 
  end
  
  def prime_factorial(prime, remainder)
    if prime?(remainder)
      @result = [prime, remainder]
    else
      new_remainder = remainder / prime
    
      if prime?(new_remainder)
        @result = [new_remainder, prime, prime]
      else
        @result = [@number]  
      end
    end
  end
end