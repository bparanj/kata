
class PrimeFinder
  
  def calculate(n)
    current_prime = 2
    result = tdad(n)
    
    while current_prime * current_prime < n
      
      
    end
    
    # while current_prime < Math.sqrt(n)
    #   cross_out_multiples_of_2(result)
    #   current_prime = 3
    #   cross_out_multiples_of_3(result)
    #   current_prime = 5
    #   cross_out_multiples_of_5(result)
    #   
    # end
  end
  
  def tdad(n)
    result = []
    2.upto(n) do |x|
      result << x
    end
    result
  end
  
  def cross_out_multiples_of_2(elements)
    elements.reject! {|e| (e % 2) == 0 if e != 2} 
  end
  
  def cross_out_multiples_of_3(elements)
    elements.reject! {|e| (e % 3) == 0 if e != 3} 
  end
  
  def cross_out_multiples_of_5(elements)
    elements.reject! {|e| (e % 5) == 0 if e != 5} 
  end
  
end

describe PrimeFinder do
  it 'should return [2] for 2' do
    pf = PrimeFinder.new
    result = pf.calculate(2)
    
    expect(result).to eq([2])
  end
  
  xit 'should return [2,3] for 4' do
    pf = PrimeFinder.new
    result = pf.calculate(4)
    
    expect(result).to eq([2,3])    
  end
  
  it 'should make a list of all the integers less than or equal to n and greater than 1' do
    pf = PrimeFinder.new
    result = pf.tdad(3)
    
    expect(result).to eq([2,3])    
  end
  
  it 'should cross out multiples of 2' do
    pf = PrimeFinder.new
    
    result = pf.cross_out_multiples_of_2([2,3,4])
    
    expect(result).to eq([2,3])
  end
  
  it 'should cross out multiples of 3' do
    pf = PrimeFinder.new
    
    result = pf.cross_out_multiples_of_3([2,3,4,5,6,7,8,9])
    
    expect(result).to eq([2,3,4,5,7,8])
  end
  
  it 'should cross out multiples of 5' do
    pf = PrimeFinder.new
    
    result = pf.cross_out_multiples_of_5([2,3,5,7,11,13,17,19,23,25,29])
    
    expect(result).to eq([2,3,5,7,11,13,17,19,23,29])    
  end
end