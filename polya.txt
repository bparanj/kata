How to Solve It suggests the following steps when solving a mathematical problem:

First, you have to understand the problem.
After understanding, then make a plan.
Carry out the plan.
Look back on your work. How could it be better?
If this technique fails, Pólya advises: "If you can't solve a problem, then there is an easier problem you can solve: find it."[7] Or: "If you cannot solve the proposed problem, try to solve first some related problem. Could you imagine a more accessible related problem?"

First principle: Understand the problem
"Understand the problem" is often neglected as being obvious and is not even mentioned in many mathematics classes. Yet students are often stymied in their efforts to solve it, simply because they don't understand it fully, or even in part. In order to remedy this oversight, Pólya taught teachers how to prompt each student with appropriate questions, depending on the situation, such as:

What are you asked to find or show?
Can you restate the problem in your own words?
Can you think of a picture or a diagram that might help you understand the problem?
Is there enough information to enable you to find a solution?
Do you understand all the words used in stating the problem?
Do you need to ask a question to get the answer?
The teacher is to select the question with the appropriate level of difficulty for each student to ascertain if each student understands at their own level, moving up or down the list to prompt each student, until each one can respond with something constructive.

Second principle: Devise a plan
Pólya mentions that there are many reasonable ways to solve problems. The skill at choosing an appropriate strategy is best learned by solving many problems. You will find choosing a strategy increasingly easy. A partial list of strategies is included:

Guess and check
Make an orderly list
Eliminate possibilities
Use symmetry
Consider special cases
Use direct reasoning
Solve an equation
Also suggested:

Look for a pattern
Draw a picture
Solve a simpler problem
Use a model
Work backward
Use a formula
Be creative
Use your head/noggin

Third principle: Carry out the plan

This step is usually easier than devising the plan. In general, all you need is care and patience, given that you have the necessary skills. Persist with the plan that you have chosen. If it continues not to work discard it and choose another. Don't be misled; this is how mathematics is done, even by professionals.

Fourth principle: Review/extend
Pólya mentions that much can be gained by taking the time to reflect and look back at what you have done, what worked and what didn't. Doing this will enable you to predict what strategy to use to solve future problems, if these relate to the original problem.

Heuristics
The book contains a dictionary-style set of heuristics, many of which have to do with generating a more accessible problem. For example:

Heuristic	Informal Description	Formal analogue
Analogy	Can you find a problem analogous to your problem and solve that?	Map
Generalization	Can you find a problem more general than your problem?	Generalization
Induction	Can you solve your problem by deriving a generalization from some examples?	Induction
Variation of the Problem	Can you vary or change your problem to create a new problem (or set of problems) whose solution(s) will help you solve your original problem?	Search
Auxiliary Problem	Can you find a subproblem or side problem whose solution will help you solve your problem?	Subgoal
Here is a problem related to yours and solved before	Can you find a problem related to yours that has already been solved and use that to solve your problem?	Pattern recognition
Pattern matching
Reduction
Specialization	Can you find a problem more specialized?	Specialization
Decomposing and Recombining	Can you decompose the problem and "recombine its elements in some new manner"?	Divide and conquer
Working backward	Can you start with the goal and work backwards to something you already know?	Backward chaining
Draw a Figure	Can you draw a picture of the problem?	Diagrammatic Reasoning
Auxiliary Elements	Can you add some new element to your problem to get closer to a solution?	Extension
The technique "have I used everything" is perhaps most applicable to formal educational examinations (e.g., n men digging m ditches) problems.

The book has achieved "classic" status because of its considerable influence.
