How to Solve It suggests the following steps when solving a mathematical problem:
---------------------------------------------------------------------------------

First, you have to understand the problem.
After understanding, make a plan.
Carry out the plan.
Look back on your work. How could it be better?

If this technique fails, Pólya advises: "If you can't solve a problem, then there is an easier problem you can solve: find it." Or: "If you cannot solve the proposed problem, try to solve first some related problem. Could you imagine a more accessible related problem?"

This is learnable and repeatable process to solve problems.

--------------------------------------------
First principle: Understand the problem
--------------------------------------------

This is often neglected as being obvious. Yet we often face difficulties in our efforts to solve problems, because we don't understand the problem. In this phase ask yourself the following questions:

Can you draw a diagram that might help you understand the problem?
Do you understand all the words used in stating the problem?
Do you need to ask a question to get the answer?
Is there enough information to enable you to find a solution?
What are you asked to find ?
Can you restate the problem in your own words?

---------------------------------
Second principle: Devise a plan
---------------------------------

There are many reasonable ways to solve problems. The skill at choosing an appropriate strategy is best learned by solving many problems. You will find choosing a strategy increasingly easy. A partial list of strategies is included:

Consider special cases
Make an orderly list
Solve an equation
Look for a pattern
Draw a picture
Solve a simpler problem
Use a model
Work backward
Use a formula

Guess and check
Eliminate possibilities
Use symmetry
Use direct reasoning

-----------------------------------
Third principle: Carry out the plan
-----------------------------------

This step is usually easier than devising the plan. If it does not work, discard it and choose another. 

-----------------------------------
Fourth principle: Review/extend
-----------------------------------

Take time to reflect and look back at what you have done, what worked and what didn't. Doing this will enable you to predict what strategy to use to solve future problems, if these relate to the original problem.


-----------------------------------
Heuristics
-----------------------------------

The book contains a dictionary-style set of heuristics, many of which have to do with generating a more accessible problem. For example:

----------------------------------------------------------------------
Heuristic	Informal Description	Formal analogue
----------------------------------------------------------------------

Decomposing and Recombining	Can you decompose the problem and "recombine its elements in some new manner"?	Divide and conquer
Induction	Can you solve your problem by deriving a generalization from some examples?	Induction
Auxiliary Problem	Can you find a subproblem or side problem whose solution will help you solve your problem?	Subgoal
Here is a problem related to yours and solved before	Can you find a problem related to yours that has already been solved and use that to solve your problem?	Pattern recognition
Working backward	Can you start with the goal and work backwards to something you already know?	Backward chaining
Draw a Figure	Can you draw a picture of the problem?	Diagrammatic Reasoning

Analogy	Can you find a problem analogous to your problem and solve that?	Map
Generalization	Can you find a problem more general than your problem?	Generalization

Variation of the Problem	Can you vary or change your problem to create a new problem (or set of problems) whose solution(s) will help you solve your original problem?	Search
Pattern matching
Reduction
Specialization	Can you find a problem more specialized?	Specialization
Auxiliary Elements	Can you add some new element to your problem to get closer to a solution?	Extension
The technique "have I used everything" is perhaps most applicable to formal educational examinations (e.g., n men digging m ditches) problems.

