require_relative '../prime_factor'

# • prime_factor_of(2) = [2] 
# • prime_factor_of(3) = [3] 
# • prime_factor_of(4) = [2,2] 
# • prime_factor_of(6) = [2,3] 
# • prime_factor_of(8) = [2,2,2] 
# • prime_factor_of(9) = [3,3] 
# • prime_factor_of(10)=[2,5] 
# • prime_factor_of(12) = [2,2,3]

describe 'Calculate prime factors of a given integer' do
  it 'should return an array consisting 2 when the input is 2' do
    prime_factor = PrimeFactor.new(2)
    result = prime_factor.calculate
    
    expect(result).to eq([2])
  end
  
  it 'should return an array consisting 3 when the input is 3' do
    prime_factor = PrimeFactor.new(3)
    result = prime_factor.calculate
    
    expect(result).to eq([3])
  end
  
  it 'should return an array consisting of 2 and 2 when the input is 4' do
    prime_factor = PrimeFactor.new(4)
    result = prime_factor.calculate
    
    expect(result).to eq([2,2])
  end
  
  it 'should return an array consisting of 2 and 3 when the input is 6' do
    prime_factor = PrimeFactor.new(6)
    result = prime_factor.calculate
    
    expect(result).to eq([2,3])
  end

  it 'should return an array consisting of 2,2,2 when the input is 8' do
    prime_factor = PrimeFactor.new(8)
    result = prime_factor.calculate
    
    expect(result).to eq([2,2,2])
  end

  it 'should return an array consisting of 3,3 when the input is 9' do
    prime_factor = PrimeFactor.new(9)
    result = prime_factor.calculate
    
    expect(result).to eq([3,3])
  end
  
  it 'should return an array consisting of 2,5 when the input is 10' do
    prime_factor = PrimeFactor.new(10)
    result = prime_factor.calculate
    
    expect(result).to eq([2,5])
  end
  
  it 'should return an array consisting of 2,2,3 when the input is 12' do
    prime_factor = PrimeFactor.new(12)
    result = prime_factor.calculate
    
    expect(result).to eq([3,2,2])
  end

  # it 'should return an array consisting of 3,7,7 when the input is 147' do
  #   prime_factor = PrimeFactor.new(147)
  #   result = prime_factor.calculate
  #   
  #   expect(result).to eq([3,7,7])
  # end

  
end