# Constraint : Do not use inject method
# Do not use for loop
# Do not use each

class Accumulator
  def initialize(list)
    @list = list
  end
  
  def calculate
    #  initialization conditions for loop and solution to summing problem when list size is 0
    index = 0
    sum = 0

    while index < @list.size
      sum = sum + @list[index]
      index = index + 1
    end
    sum
  end
  
end