require_relative 'accumulator'

describe Accumulator do
  it 'should return 0 for empty list of numbers' do
    accumulator = Accumulator.new([])
    
    result = accumulator.calculate
    
    expect(result).to eq(0)
  end
  
  it 'should return 1 for a list containing 1' do
    accumulator = Accumulator.new([1])
    
    result = accumulator.calculate
    
    expect(result).to eq(1)
  end
  
  it 'should return 3 for a list containing 1 and 2' do
    accumulator = Accumulator.new([1,2])
    
    result = accumulator.calculate
    
    expect(result).to eq(3)
  end
end