# 1. Allocate responsibilities to the right class.
#     digest method should not return any value, digest method does not belong to the Cheese class.
# 2. Do not violate Law of Demeter.
# 3. Separate Command from Query. 
#      digest method is a command. 

class Walrus
  attr_reader :energy
  
  def initialize
    @energy = 0
  end
  
  def eat(food)
    digest
    @energy += food.energy
  end
  
  private
  
  def digest
    p 'Digesting...'
  end
end

class Cheese
  def energy
    100
  end
end



cheese = Cheese.new
w  = Walrus.new

p "Energy before eating is : #{w.energy}"

w.eat(cheese)

p "Energy now is : #{w.energy}"